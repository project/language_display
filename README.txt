
----------------------------------
LANGUAGE DISPLAY
----------------------------------

The language formatter is still hard-coded in core.
For this module to work you need to hack the core using this patch;
https://www.drupal.org/node/2637808#comment-12091851
