<?php

namespace Drupal\Tests\language_display\Functional;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\node\NodeInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the provided field formatters.
 *
 * @group language_display
 */
class FormatterPluginsTest extends BrowserTestBase {

  /**
   * An administrative user to configure the test environment.
   */
  protected $adminUser;

  /**
   * The profile to install as a basis for testing.
   *
   * @var string
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['language_display', 'content_translation', 'node', 'field_ui'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer content types',
      'administer node fields',
      'administer node display',
    ]);

    // Add and configure extra languages.
    ConfigurableLanguage::createFromLangcode('fr')->save();
    ConfigurableLanguage::createFromLangcode('de')->save();
    $this->config('language.negotiation')->set('url', [
      'source' => 'path_prefix',
      'prefixes' => [
        'en' => 'en',
        'fr' => 'fr',
        'de' => 'de',
      ],
    ])->save();

    // Create the Page content type.
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Page',
    ]);

    // Enable translation for page.
    ContentLanguageSettings::loadByEntityTypeBundle('node', 'page')
      ->setDefaultLangcode('en')
      ->setLanguageAlterable(TRUE)
      ->setThirdPartySetting('content_translation', 'enabled', TRUE)
      ->save();

    // Rebuild the container so that the new languages are picked up by services
    // that hold a list of languages.
    $this->rebuildContainer();
  }

  /**
   * Test original_language formatter's rendering.
   */
  public function testOriginalLanguageFormatter() {

    $this->drupalLogin($this->adminUser);

    $node = $this->prepareSampleNodeAndRunSanityTests();

    // Activate language_display formatter; set to original language.
    $edit = [
      'fields[langcode][region]' => 'content',
      'fields[langcode][type]' => 'original_language',
    ];
    $this->drupalGet('admin/structure/types/manage/page/display');
    $this->assertSession()->fieldValueEquals('fields[langcode][type]', 'language');
    $this->submitForm($edit, 'Save');
    $component = EntityViewDisplay::load('node.page.default')->getComponent('langcode');

    // The original language should be 'French', for the english translation.
    Cache::invalidateTags($node->getCacheTags());
    $this->drupalGet('node/' . $node->id());
    $langcodeContainer = $this->assertSession()->elementExists('css', 'div.field--name-langcode.field--type-language');
    $langcodeItem = $this->assertSession()->elementExists('css', 'div.field__item', $langcodeContainer);
    $this->assertEquals('French', $langcodeItem->getText());
  }

  /**
   * Test original_language_translation_counter formatter's rendering.
   */
  public function testOriginalLanguageTranslationCounterFormatter() {

    $this->drupalLogin($this->adminUser);

    $node = $this->prepareSampleNodeAndRunSanityTests();

    // Activate language_display formatter; set to original_language_translation_counter.
    $edit = [
      'fields[langcode][region]' => 'content',
      'fields[langcode][type]' => 'original_language_translation_counter',
    ];
    $this->drupalGet('admin/structure/types/manage/page/display');
    $this->assertSession()->fieldValueEquals('fields[langcode][type]', 'language');
    $this->submitForm($edit, 'Save');
    $component = EntityViewDisplay::load('node.page.default')->getComponent('langcode');

    // The original language should be 'French', for the english translation, with a count of 1 translation.
    Cache::invalidateTags($node->getCacheTags());
    $this->drupalGet('node/' . $node->id());
    $langcodeContainer = $this->assertSession()->elementExists('css', 'div.field--name-langcode.field--type-language');
    $langcodeItemLabel = $this->assertSession()->elementExists('css', 'div.field__item span.language-name', $langcodeContainer);
    $this->assertEquals('French', $langcodeItemLabel->getText());
    $langcodeItemCounter = $this->assertSession()->elementExists('css', 'div.field__item span.translation-counter', $langcodeContainer);
    $this->assertEquals('1 translation', $langcodeItemCounter->getText());

    // Add an extra translation, in german.
    $node->addTranslation('de',['title' => $this->randomString() . '_de'])->save();
    // The original language should be 'French', for the german translation, with a count of 1 translation.
    Cache::invalidateTags($node->getCacheTags());
    $this->drupalGet('node/' . $node->id(), ['language' => ConfigurableLanguage::load('de')]);
    $langcodeContainer = $this->assertSession()->elementExists('css', 'div.field--name-langcode.field--type-language');
    $langcodeItemLabel = $this->assertSession()->elementExists('css', 'div.field__item span.language-name', $langcodeContainer);
    $this->assertEquals('French', $langcodeItemLabel->getText());
    $langcodeItemCounter = $this->assertSession()->elementExists('css', 'div.field__item span.translation-counter', $langcodeContainer);
    $this->assertEquals('2 translations', $langcodeItemCounter->getText());
  }

  /**
   * Prepares a sample node for running formatter tests.
   *
   * Also runs some sanity assertions.
   */
  protected function prepareSampleNodeAndRunSanityTests(): NodeInterface {
    // Display language field on page.
    $edit = [
      'fields[langcode][region]' => 'content',
      'fields[langcode][type]' => 'language',
    ];
    $this->drupalGet('admin/structure/types/manage/page/display');
    $this->submitForm($edit, 'Save');

    // Create a french node with an english translation.
    $title = $this->randomString();
    $this->drupalCreateNode(['type' => 'page', 'title' => $title . '_fr', 'langcode' => 'fr']);
    $node = $this->drupalGetNodeByTitle($title . '_fr');
    $node->addTranslation('en',['title' => $title . '_en'])->save();

    // Visit the english translation page.
    Cache::invalidateTags($node->getCacheTags());
    $this->drupalGet('node/' . $node->id());
    $titleContainer = $this->assertSession()->elementExists('css', 'h1.title.page-title');
    $this->assertEquals($titleContainer->getText(), $title . '_en');
    $langcodeContainer = $this->assertSession()->elementExists('css', 'div.field--name-langcode.field--type-language');
    $langcodeItem = $this->assertSession()->elementExists('css', 'div.field__item', $langcodeContainer);
    $this->assertEquals('English', $langcodeItem->getText());

    // Visit the french translation page.
    Cache::invalidateTags($node->getCacheTags());
    $this->drupalGet('node/' . $node->id(), ['language' => ConfigurableLanguage::load('fr')]);
    $titleContainer = $this->assertSession()->elementExists('css', 'h1.title.page-title');
    $this->assertEquals($titleContainer->getText(), $title . '_fr');
    $langcodeContainer = $this->assertSession()->elementExists('css', 'div.field--name-langcode.field--type-language');
    $langcodeItem = $this->assertSession()->elementExists('css', 'div.field__item', $langcodeContainer);
    $this->assertEquals('French', $langcodeItem->getText());

    return $node;
  }

}
